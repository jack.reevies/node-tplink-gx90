const CryptoJS = require("crypto-js")
const rsa = require('node-bignumber')
const crypto = require('crypto')

// These could be generated each time but its not necessary
const aesKey = "3136399396688174"
const aesIV = "2749023817744124"

function decryptResponse(aesKey, aesIV, encryptedResponse) {
  const iv = CryptoJS.enc.Utf8.parse(aesIV)
  const key = CryptoJS.enc.Utf8.parse(aesKey)
  const opts = { iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }
  const raw = CryptoJS.AES.decrypt(encryptedResponse, key, opts).toString(CryptoJS.enc.Utf8)
  const json = JSON.parse(raw)
  return json
}

function aesEncryptData(aesKey, aesIV, data) {
  const iv = CryptoJS.enc.Utf8.parse(aesIV)
  const key = CryptoJS.enc.Utf8.parse(aesKey)
  const opts = { iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }
  return CryptoJS.AES.encrypt(data, key, opts).toString()
}

function rsaEncryptData(n, e, data) {
  const key = new rsa.Key();
  key.setPublic(n.toLowerCase(), e);
  return key.encrypt(data)
}

function getSignatureAes(nn, e, hash, seq, aesString) {
  const r = aesString + "&h=" + hash + "&s=" + seq
  for (var n = "", o = 0; o < r.length;) {
    n += rsaEncryptData(nn, e, r.substring(o, o + 53))
    o += 53
  }
  return n
}

function getSignature(nn, e, hash, seq) {
  const r = "h=" + hash + "&s=" + seq;
  return rsaEncryptData(nn, e, r)
}

async function makeRequest(aesKey, aesIV, n, e, seq, token, sessionid, hash, url, data) {
  const encData = aesEncryptData(aesKey, aesIV, data)
  const signedData = getSignature(n, e, hash, seq + encData.length)

  const res = await fetch(`http://tplinkwifi.net/cgi-bin/luci/;stok=${token}/admin/${url}`, {
    "headers": { ...getHeaders(), "cookie": `sysauth=${sessionid}`, },
    "body": `sign=${signedData}&data=${encodeURIComponent(encData)}`,
    "method": "POST"
  });

  const json = await res.json()
  return decryptResponse(aesKey, aesIV, json.data).data
}

async function getDevices(aesKey, aesIV, n, e, seq, token, sessionid, hash) {
  return await makeRequest(aesKey, aesIV, n, e, seq, token, sessionid, hash, 'smart_network?form=game_accelerator', 'operation=loadDevice')
}

async function login(loginKey, aesKey, aesIV, seq, password, n, e) {
  const rsaPassword = rsaEncryptData(loginKey.n, loginKey.e, password)
  const data = `password=${rsaPassword}&operation=login`
  const encData = aesEncryptData(aesKey, aesIV, data)
  const hash = getHash(password)
  const signedData = getSignatureAes(n, e, hash, seq + encData.length, `k=${aesKey}&i=${aesIV}`)

  const res = await fetch("http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=login", {
    "headers": getHeaders(),
    "body": `sign=${signedData}&data=${encodeURIComponent(encData)}`,
    "method": "POST"
  });

  const json = await res.json()
  const decryptedJson = decryptResponse(aesKey, aesIV, json.data)

  const setCookie = res.headers.get('set-cookie')
  const sessionId = setCookie.split(';')[0].split('=')[1]

  return { stok: decryptedJson.data.stok, sessionid: sessionId, hash }
}

function getHash(password) {
  return crypto.createHash('md5').update(`admin${password}`).digest("hex")
}

function getHeaders() {
  return {
    "accept": "application/json, text/javascript, */*; q=0.01",
    "accept-language": "en-GB,en;q=0.6",
    "cache-control": "no-cache",
    "content-type": "application/x-www-form-urlencoded",
    "pragma": "no-cache",
    "sec-gpc": "1",
    "x-requested-with": "XMLHttpRequest",
    "Referer": "http://tplinkwifi.net/webpages/index.html?t=6ee9015e",
    "Referrer-Policy": "strict-origin-when-cross-origin"
  }
}

async function getRsaKeyAndSec() {
  const res = await fetch("http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=auth", {
    "headers": getHeaders(),
    "body": "operation=read",
    "method": "POST"
  });

  const json = await res.json()
  return { n: json.data.key[0], e: json.data.key[1], seq: json.data.seq }
}

async function getLoginEncryptKey() {
  const res = await fetch("http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=keys", {
    "headers": getHeaders(),
    "body": "operation=read",
    "method": "POST"
  });

  const json = await res.json()
  return { n: json.data.password[0], e: json.data.password[1] }
}

async function start() {
  const loginKey = await getLoginEncryptKey()
  const postLoginKey = await getRsaKeyAndSec()
  const loginResults = await login(loginKey, aesKey, aesIV, postLoginKey.seq, 'PASSWORD', postLoginKey.n, postLoginKey.e)
  const devices = await getDevices(aesKey, aesIV, postLoginKey.n, postLoginKey.e, postLoginKey.seq, loginResults.stok, loginResults.sessionid, loginResults.hash)

  console.log(devices)
}

start()